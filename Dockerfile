FROM node:alpine

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

COPY . /usr/src/app/
RUN corepack enable
RUN yarn set version stable
RUN yarn install
RUN yarn build

CMD ["yarn", "start"]
