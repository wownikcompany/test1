'use client';

import { makeAutoObservable, reaction, runInAction } from "mobx";
import { Point } from "@/models/Point";
import { Track } from "@/models/Track";
import { getData } from "@/services/dataService";

class ChartStore {
    isLoading = false
    pointsCount: number = 0;

    points: Array<Point> = [];
    tracks: Array<Track> = [];

    xAxisMin: number = 0;
    xAxisMax: number = 100;

    constructor() {
        makeAutoObservable(this);

        reaction(() => this.pointsCount, this.loadData);
        this.setPointsCount(30);
    }

    get maxXAxisMax(): number {
        return this.tracks.reduce((accumulator, track) => accumulator + track.distance, 0)
    }

    setXAxisMin = (value: number) => {
        this.xAxisMin = value;
    }

    setXAxisMax = (value: number) => {
        this.xAxisMax = value;
    }

    setPointsCount = (count: number): void => {
        this.pointsCount = count;
    }

    loadData = async (): Promise<void> => {
        if (this.isLoading) {
            return;
        }

        this.isLoading = true;

        while (this.points.length !== this.pointsCount) {
            const {points, tracks} = await getData(this.pointsCount);

            runInAction(() => {
                this.points = points;
                this.tracks = tracks;
            });
        }
        runInAction(() => {
            this.isLoading = false;
        });
    }
}

export default ChartStore;