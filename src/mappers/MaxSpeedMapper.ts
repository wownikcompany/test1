import { MaxSpeed } from "@/enums/MaxSpeed";
import { MaxSpeedView } from "@/enums/MaxSpeedView";

export function mapMaxSpeed(maxSpeed: MaxSpeed): MaxSpeedView {
    switch (maxSpeed) {
        case MaxSpeed.SLOW:
            return MaxSpeedView.SLOW;
        case MaxSpeed.NORMAL:
            return MaxSpeedView.NORMAL;
        case MaxSpeed.FAST:
            return MaxSpeedView.FAST;
    }
}