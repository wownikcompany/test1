import { Surface } from "@/enums/Surface";
import { SurfaceView } from "@/enums/SurfaceView";

export function mapSurface(surface: Surface): SurfaceView {
    switch (surface) {
        case Surface.SAND:
            return SurfaceView.SAND;
        case Surface.ASPHALT:
            return SurfaceView.ASPHALT;
        case Surface.GROUND:
            return SurfaceView.GROUND;
    }
}