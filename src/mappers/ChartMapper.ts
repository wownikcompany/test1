import { Point } from "@/models/Point";
import { Track } from "@/models/Track";
import { ChartPoint } from "@/models/ChartPoint";
import { Surface } from "@/enums/Surface";
import { MaxSpeed } from "@/enums/MaxSpeed";
import { DataTypes } from "@/enums/DataTypes";

function getYAxisMax(points: Array<Point>) {
    return Math.max(...points.map((point: Point) => point.height)) * 1.1;
}

export function mapIntoChartData(points: Array<Point>, tracks: Array<Track>): ChartData {
    const pointsMap = getPointsMap(points);
    const yAxisMax: number = getYAxisMax(points);

    let chartPoints: Array<ChartPoint> = [];

    let trackStartX: number = 0;
    for (let i: number = 0; i < tracks.length; i++) {
        const t = [tracks[i - 1], tracks[i], tracks[i + 1]];
        const points: Array<ChartPoint> = getChartPointsForTrack(trackStartX, pointsMap, yAxisMax, t);
        chartPoints = [...chartPoints, ...points];

        trackStartX += tracks[i].distance;
    }

    return {chartPoints, yAxisMax, xAxisMax: trackStartX};
}

function getPointsMap(points: Array<Point>): Record<string, Point> {
    const pointsMap: Record<string, Point> = new Map();
    points.forEach((point: Point) => pointsMap.set(point.id, point));
    return pointsMap;
}

function getChartPointsForTrack(startX: number, pointsMap: Record<string, Point>, yAxisMax: number, tracks: Array<Track>): Array<ChartPoint> {
    const [prevTrack, currentTrack, nextTrack] = tracks;

    if (!currentTrack) {
        return [];
    }

    const firstPoint: Point = pointsMap.get(currentTrack.firstId);
    const secondPoint: Point = pointsMap.get(currentTrack.secondId);
    const middlePoint: Point = {height: (firstPoint.height + secondPoint.height) / 2}

    const firstX: number = startX;
    const secondX: number = startX + currentTrack.distance;
    const middleX: number = (firstX + secondX) / 2;

    const prevTrackIsFAST: boolean = prevTrack?.maxSpeed === MaxSpeed.FAST;
    const prevTrackIsNORMAL: boolean = prevTrack?.maxSpeed === MaxSpeed.NORMAL;
    const prevTrackIsSLOW: boolean = prevTrack?.maxSpeed === MaxSpeed.SLOW;
    const prevTrackIsSAND: boolean = prevTrack?.surface === Surface.SAND;
    const prevTrackIsASPHALT: boolean = prevTrack?.surface === Surface.ASPHALT;
    const prevTrackIsGround: boolean = prevTrack?.surface === Surface.GROUND;

    const currentTrackIsFAST: boolean = currentTrack.maxSpeed === MaxSpeed.FAST;
    const currentTrackIsNORMAL: boolean = currentTrack.maxSpeed === MaxSpeed.NORMAL;
    const currentTrackIsSLOW: boolean = currentTrack.maxSpeed === MaxSpeed.SLOW;
    const currentTackIsSand: boolean = currentTrack.surface === Surface.SAND;
    const currentTrackIsASPHALT: boolean = currentTrack.surface === Surface.ASPHALT;
    const currentTrackIsGround: boolean = currentTrack.surface === Surface.GROUND;

    const nextTrackIsFAST: boolean = nextTrack?.maxSpeed === MaxSpeed.FAST;
    const nextTrackIsNORMAL: boolean = nextTrack?.maxSpeed === MaxSpeed.NORMAL;
    const nextTrackIsSLOW: boolean = nextTrack?.maxSpeed === MaxSpeed.SLOW;
    const nextTrackIsSAND: boolean = nextTrack?.surface === Surface.SAND;
    const nextTrackIsASPHALT: boolean = nextTrack?.surface === Surface.ASPHALT;
    const nextTrackIsGround: boolean = nextTrack?.surface === Surface.GROUND;

    const firstChartPoint: ChartPoint = {
        x: firstX,
        [DataTypes.HEIGHT]: firstPoint.height,
        [DataTypes.FAST]: prevTrackIsFAST || currentTrackIsFAST ? firstPoint.height : null,
        [DataTypes.NORMAL]: prevTrackIsNORMAL || currentTrackIsNORMAL ? firstPoint.height : null,
        [DataTypes.SLOW]: prevTrackIsSLOW || currentTrackIsSLOW ? firstPoint.height : null,
        [DataTypes.SAND]: prevTrackIsSAND || currentTackIsSand ? yAxisMax : null,
        [DataTypes.ASPHALT]: prevTrackIsASPHALT || currentTrackIsASPHALT ? yAxisMax : null,
        [DataTypes.GROUND]: prevTrackIsGround || currentTrackIsGround ? yAxisMax : null
    }

    const middleChartPoint: ChartPoint = {
        x: middleX,
        [DataTypes.HEIGHT]: null,
        [DataTypes.FAST]: currentTrackIsFAST ? middlePoint.height : null,
        [DataTypes.NORMAL]: currentTrackIsNORMAL ? middlePoint.height : null,
        [DataTypes.SLOW]: currentTrackIsSLOW ? middlePoint.height : null,
        [DataTypes.SAND]: currentTackIsSand ? yAxisMax : null,
        [DataTypes.ASPHALT]: currentTrackIsASPHALT ? yAxisMax : null,
        [DataTypes.GROUND]: currentTrackIsGround ? yAxisMax : null
    }

    if (nextTrack) {
        return [firstChartPoint, middleChartPoint];
    }

    const secondChartPoint: ChartPoint = {
        x: secondX,
        [DataTypes.HEIGHT]: secondPoint.height,
        [DataTypes.FAST]: nextTrackIsFAST || currentTrackIsFAST ? secondPoint.height : null,
        [DataTypes.NORMAL]: nextTrackIsNORMAL || currentTrackIsNORMAL ? secondPoint.height : null,
        [DataTypes.SLOW]: nextTrackIsSLOW || currentTrackIsSLOW ? secondPoint.height : null,
        [DataTypes.SAND]: nextTrackIsSAND || currentTackIsSand ? yAxisMax : null,
        [DataTypes.ASPHALT]: nextTrackIsASPHALT || currentTrackIsASPHALT ? yAxisMax : null,
        [DataTypes.GROUND]: nextTrackIsGround || currentTrackIsGround ? yAxisMax : null
    }

    return [firstChartPoint, middleChartPoint, secondChartPoint];
}