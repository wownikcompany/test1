import { Point } from "@/models/Point";
import { Track } from "@/models/Track";
import { Surface } from "@/enums/Surface";
import { MaxSpeed } from "@/enums/MaxSpeed";
import { getRandomValue } from "@/helpers/randomHelper";


function generateHeight(baseHeight: number): number {
    const height = baseHeight + (0.5 - Math.random()) * 100;
    return height < 0 ? -height : height;
}

function generateDistance(): number {
    return (Math.random() + 0.1) * 1000;
}

function generateSurface(): number {
    const list = [Surface.SAND, Surface.ASPHALT, Surface.GROUND];
    return getRandomValue(list);
}

function generateSpeed(): number {
    const list = [MaxSpeed.SLOW, MaxSpeed.NORMAL, MaxSpeed.FAST];
    return getRandomValue(list);
}

function generatePoint(id: number, baseHeight: number): Point {
    return {
        id: id,
        name: "Point " + id,
        height: generateHeight(baseHeight),
    }
}

function generateTrack(firstPointId: number, secondPointId: number): Track {
    return {
        firstId: firstPointId,
        secondId: secondPointId,
        distance: generateDistance(),
        surface: generateSurface(),
        maxSpeed: generateSpeed(),
    }
}

export function generatePoints(countOfPoints: number): Array<Point> {
    let height = 200;

    const result: Array<Point> = [];
    for (let i = 0; i < countOfPoints; i++) {
        height = generateHeight(height);
        result.push(generatePoint(i, height));
    }
    return result;
}

export function generateTracks(countOfPoints: number): Array<Track> {
    const result: Array<Track> = [];
    for (let i = 0; i < countOfPoints - 1; i++) {
        const item = generateTrack(i, i + 1);
        result.push(item);
    }
    return result;
}
