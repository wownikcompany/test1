import { DataTypes } from "@/enums/DataTypes";

export const keyToLabel: Record<DataTypes, string> = {
    [DataTypes.FAST]: "Быстро",
    [DataTypes.NORMAL]: "Нормально",
    [DataTypes.SLOW]: "Медленно",
    [DataTypes.SAND]: "Песок",
    [DataTypes.ASPHALT]: "Асфальт",
    [DataTypes.GROUND]: "Земля",
}

export const keyToArea: Record<DataTypes, boolean> = {
    [DataTypes.FAST]: false,
    [DataTypes.NORMAL]: false,
    [DataTypes.SLOW]: false,
    [DataTypes.SAND]: true,
    [DataTypes.ASPHALT]: true,
    [DataTypes.GROUND]: true,
}

export const keyToColors: Record<DataTypes, string> = {
    [DataTypes.FAST]: "rgb(255, 0, 0)",
    [DataTypes.NORMAL]: "rgb(0, 0, 255)",
    [DataTypes.SLOW]: "rgb(255, 200, 0)",
    [DataTypes.SAND]: "rgb(255, 255, 140)",
    [DataTypes.ASPHALT]: "rgb(190, 190, 190)",
    [DataTypes.GROUND]: "rgb(170, 255, 170)",
}

export const keyToShowMark: Record<DataTypes, boolean> = {
    [DataTypes.FAST]: ({index}) => index % 2 === 0,
    [DataTypes.NORMAL]: ({index}) => index % 2 === 0,
    [DataTypes.SLOW]: ({index}) => index % 2 === 0,
    [DataTypes.SAND]: false,
    [DataTypes.ASPHALT]: false,
    [DataTypes.GROUND]: false,
}