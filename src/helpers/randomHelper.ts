export function getRandomValue<T>(list: Array<T>): T {
    const randValue = Math.floor(Math.random() * list.length);
    return list[randValue];
}