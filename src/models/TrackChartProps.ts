import { Point } from "@/models/Point";
import { Track } from "@/models/Track";

export interface TrackChartProps {
    points: Array<Point>,
    tracks: Array<Track>,
    xAxisMin?: number,
    xAxisMax?: number
}