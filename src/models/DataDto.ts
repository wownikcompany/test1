interface DataDto {
    points: Array<Point>;
    tracks: Array<Track>;
}