import { MaxSpeedView } from "@/enums/MaxSpeedView";
import { SurfaceView } from "@/enums/SurfaceView";

export interface ChartPoint {
    x: number,
    [MaxSpeedView.FAST]: number | null,
    [MaxSpeedView.NORMAL]: number | null,
    [MaxSpeedView.SLOW]: number | null,
    [SurfaceView.SAND]: number | null,
    [SurfaceView.ASPHALT]: number | null,
    [SurfaceView.GROUND]: number | null,
}