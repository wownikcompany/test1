interface ChartData{
    chartPoints: Array<ChartPoint>;
    yAxisMax: number;
    xAxisMax: number;
}