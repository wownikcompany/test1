import { sleep } from "@/helpers/sleepHelper";
import { generatePoints, generateTracks } from "@/dataGenerators/trackGenerator";

export async function getData(countOfPoints: number): Promise<DataDto> {
    await sleep(1000);

    const points = generatePoints(countOfPoints);
    const tracks = generateTracks(countOfPoints);
    return {
        points,
        tracks
    }
}