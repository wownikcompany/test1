export enum DataTypes {
    FAST="FAST",
    NORMAL="NORMAL",
    SLOW="SLOW",
    SAND="SAND",
    ASPHALT="ASPHALT",
    GROUND="GROUND",
}