// Список возможных максимальных скоростей
export enum MaxSpeed {
    FAST,
    NORMAL,
    SLOW
}