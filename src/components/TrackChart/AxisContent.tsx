import React from "react";
import { createUseStyles } from 'react-jss'
import { ChartsAxisContentProps } from "@mui/x-charts";
import { Card, CardContent, Typography } from "@mui/material";
import { DataTypes } from "@/enums/DataTypes";

const useStyles = createUseStyles({
    card: {
        margin: {
            top: 5,
            right: '0.1rem',
            bottom: 0,
            left: '1rem'
        }
    },
    cardContent: {
        paddingBottom: 5
    }
})

const AxisContent = (props: ChartsAxisContentProps) => {
    const classes = useStyles()

    const {dataIndex, axisValue} = props;
    const fastData = props.series.filter(x => x.id === DataTypes.FAST)[0].data[dataIndex];
    const normalData = props.series.filter(x => x.id === DataTypes.NORMAL)[0].data[dataIndex];
    const slowData = props.series.filter(x => x.id === DataTypes.SLOW)[0].data[dataIndex];

    const index: number = dataIndex % 2 === 0 ? dataIndex / 2 + 1 : null;
    const distance: number = Math.round(axisValue);
    const height: number = Math.round(fastData || normalData || slowData);

    return (
        <Card variant="outlined" className={classes.card}>
            <CardContent className={classes.cardContent}>
                <Typography variant="body2">
                    {index ? `Точка: ${index}` : null}
                    <br/>
                    {`Дистанция: ${distance}`}
                    <br/>
                    {`Высота: ${height}`}
                </Typography>
            </CardContent>
        </Card>
    );
}

export default AxisContent;