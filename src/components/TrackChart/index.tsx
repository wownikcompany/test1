import React from "react";
import { AxisConfig, LineChart, LineChartProps, LineSeriesType } from '@mui/x-charts';
import { keyToArea, keyToColors, keyToLabel, keyToShowMark } from "@/helpers/chartViewSettings";
import { mapIntoChartData } from "@/mappers/ChartMapper";
import AxisContent from "@/components/TrackChart/AxisContent";
import { TrackChartProps } from "@/models/TrackChartProps";

const TrackChart = ({points, tracks, xAxisMin, xAxisMax}: TrackChartProps) => {
    const {chartPoints: dataset, yAxisMax, xAxisMax: defaultXAxisMax} = mapIntoChartData(points, tracks);

    const series: Array<LineSeriesType> = Object.keys(keyToLabel)
        .map((key: string) => {
            return {
                id: key,
                dataKey: key,
                label: keyToLabel[key],
                type: "line",
                area: keyToArea[key],
                color: keyToColors[key],
                showMark: keyToShowMark[key],
                disableHighlight: true,
            }
        });

    const xAxis: Array<AxisConfig> = [
        {
            id: "x",
            dataKey: "x",
            min: xAxisMin || 0,
            max: xAxisMax || defaultXAxisMax,
        }
    ];

    const yAxis: Array<AxisConfig> = [
        {
            id: "y",
            max: yAxisMax,
        }
    ];

    const config: LineChartProps = {
        margin: {top: 100, left: 50, right: 50, bottom: 30},
        height: 300,
        dataset,
        series,
        xAxis,
        yAxis,
        tooltip: {
            axisContent: AxisContent,
        },
    };

    return <LineChart {...config}/>;
}

export default TrackChart;