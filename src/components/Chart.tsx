'use client';

import React from "react";
import { observer } from "mobx-react-lite";
import { createUseStyles } from 'react-jss'
import { CircularProgress, Slider } from "@mui/material";
import TrackChart from "@/components/TrackChart";
import ChartStore from "@/stores/chartStore";

const useStyles = createUseStyles({
    chartWrapper: {
        height: 300
    },
    sliderWrapper: {
        width: "80%",
        maxWidth: 600,
        margin: {
            left: "auto",
            right: "auto"
        }
    },
    flexContainer: {
        width: "100%",
        padding: "1rem",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    }
})

const Chart = ({store}: { store: ChartStore }) => {
    const classes = useStyles()

    const onChangePointsCount = (event: Event, value: number) => {
        store.setPointsCount(Number(value));
    }

    const onChangeXAxisRange = (event: Event, value: number[]) => {
        const [min, max] = value;
        store.setXAxisMin(Number(min));
        store.setXAxisMax(Number(max));
    }

    const renderTrackChart = () => {
        if (store.isLoading) {
            return <CircularProgress/>;
        }

        const xAxisMin = store.xAxisMin / 100 * store.maxXAxisMax;
        const xAxisMax = store.xAxisMax / 100 * store.maxXAxisMax;

        return (
            <TrackChart points={store.points} tracks={store.tracks} xAxisMin={xAxisMin} xAxisMax={xAxisMax}/>
        );
    }

    const pointsCountMarks = [
        {
            value: 0,
            label: '0 точек',
        },
        {
            value: 100,
            label: '100',
        },
    ]

    const xAxisMarks = [
        {
            value: 0,
            label: 'Смещение 0%',
        },
        {
            value: 100,
            label: '100%',
        },
    ]

    return (
        <React.Fragment>
            <div className={classes.chartWrapper}>
                {renderTrackChart()}
            </div>
            <div className={classes.flexContainer}>
                <div className={classes.sliderWrapper}>
                    <Slider
                        value={store.pointsCount}
                        onChange={onChangePointsCount}
                        valueLabelDisplay="auto"
                        marks={pointsCountMarks}
                    />
                </div>
            </div>
            <div className={classes.flexContainer}>
                <div className={classes.sliderWrapper}>
                    <Slider
                        value={[store.xAxisMin, store.xAxisMax]}
                        onChange={onChangeXAxisRange}
                        valueLabelDisplay="auto"
                        marks={xAxisMarks}
                    />
                </div>
            </div>
            <pre>
                {JSON.stringify({points: store.points, tracks: store.tracks}, null, 2)}
            </pre>
        </React.Fragment>
    );
}

export default observer(Chart);