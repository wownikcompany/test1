'use client';

import React, { useState } from "react";
import Chart from "@/components/Chart";
import ChartStore from "@/stores/chartStore";

function Home() {
    const [store] = useState(new ChartStore());

    return (
        <main>
            <Chart store={store}/>
        </main>
    );
}

export default Home;
